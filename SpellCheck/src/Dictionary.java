import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;

/**
 * The Dictionary for spell check.
 * @author : saif
 * @date : 21/03/15
 */
public class Dictionary {

    private HashSet<String> dictionary;
    private Scanner dictCreator;

    /**
     * Constructs the dictionary
     * @param dictFile dictionary file
     */
    public Dictionary(File dictFile) {

        this.dictionary = new HashSet<String>();

        try {
            dictCreator = new Scanner(dictFile);
            while (dictCreator.hasNext()) {
                dictionary.add(dictCreator.nextLine());
            }
        } catch (FileNotFoundException e) {
            if (dictFile.getName().equals("words")) {
                System.err.println("Default dictionary file [" + dictFile.getName() + "] does not exists.");
                System.err.println(e.getMessage());
                System.err.print(FILE_ERROR.DEFAULT.getMsg());
            } else {
                System.err.println("Dictionary file [" + dictFile.getName() + "] not found");
                System.err.println(e.getMessage());
                System.err.println(FILE_ERROR.EXTERNAL.getMsg());
            }
            System.exit(EXIT_CODE.FILE_Error.getNumVal());
        } finally {
            dictCreator.close();
        }
    }

    /**
     * Prints the dictionary (no particular order)
     */
    public void printDict() {

        Iterator<String> itr = dictionary.iterator();
        System.out.println("The DICTIONARY - ");
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }
    }

    /**
     * Checks if a word exists in the dictionary
     * @param word the word to be spell checked.
     * @return true if the spelling is right else false.
     */
    public boolean Check(String word) {

        return dictionary.contains(word);
    }
}
