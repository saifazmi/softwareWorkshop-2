import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * The Document to be spell checked
 * @author : saif
 * @date : 23/03/15
 */
public class Document {

    private File fileToCheck;
    private Dictionary dict;
    private BufferedReader doc;
    int exitCode = EXIT_CODE.NO_Error.getNumVal();

    /**
     * Constructs the data structure to process the document.
     * @param fileToCheck file to be spell checked.
     * @param dict the dictionary to check with.
     */
    public Document(File fileToCheck, Dictionary dict) {

        this.fileToCheck = fileToCheck;
        this.dict = dict;
        try {
            this.doc = new BufferedReader(new FileReader(fileToCheck));
            spellCheck();
        } catch (FileNotFoundException e) {
            System.err.println("Document file [" + fileToCheck.getName() + "] not found.");
            System.err.println(e.getMessage());
            System.err.println(FILE_ERROR.EXTERNAL.getMsg());

            System.exit(EXIT_CODE.IO_Error.getNumVal());
        } finally {
            try {
                doc.close();
            } catch (IOException e) {
                System.err.println("Error while closing file [" + fileToCheck.getName() + "]");
                System.err.println(e.getMessage());

                System.exit(EXIT_CODE.IO_Error.getNumVal());
            }
            System.exit(exitCode);
        }
    }

    /**
     * Checks every word in the document with the dictionary
     */
    private void spellCheck() {

        try {
            int lineCount = 0;
            String currentLine;
            ArrayList<String> words;
            ArrayList<String> spellErrors = new ArrayList<String>();

            while ((currentLine = doc.readLine()) != null) {
                boolean errorFound = false;
                lineCount++;
                words = wordList(currentLine);

                for (int i = 0; i < words.size(); i++) {

                    if (!dict.Check(words.get(i)) && !words.get(0).equals("")) {
                        spellErrors.add(words.get(i));
                        errorFound = true;
                        exitCode = EXIT_CODE.SPELL_Error.getNumVal();
                    }
                }

                if (errorFound) {

                    System.out.print("[" + lineCount + "]");
                    System.out.println(" " + currentLine);

                    for (int i = 0; i < spellErrors.size(); i++) {
                        System.out.print(spellErrors.get(i) + " ");
                    }
                    System.out.println("\n");
                    spellErrors.clear();
                }
            }
        } catch (IOException e) {
            System.err.println("IO error: " + fileToCheck.getName());
            System.err.println(e.getMessage());

            System.exit(EXIT_CODE.IO_Error.getNumVal());
        }
    }

    /**
     * Converts a give line of text into a list of words without punctuations.
     * @param line the line of String to be converted.
     * @return an ArrayList String (words).
     */
    private ArrayList<String> wordList(String line) {

        ArrayList<String> list = new ArrayList<String>();
        Pattern punctuations = Pattern.compile("(?!\')\\p{Punct}", Pattern.UNICODE_CHARACTER_CLASS);
        String strippedLine = line.replaceAll(punctuations.toString(), "");
        for (int i = 0; i < strippedLine.split("\\s").length; i++) {
            list.add(strippedLine.split("\\s")[i]);
        }
        return list;
    }
}
