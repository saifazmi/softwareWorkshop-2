/**
 * File Error messages for Spell Check
 * @author : saif
 * @date : 23/03/15
 */
public enum FILE_ERROR {

    DEFAULT("Please provide a dictionary file of your own.\n" +
            "USAGE: java SpellCheck <file to be checked> <dictionary>"),

    EXTERNAL("Please check file name and path");

    private String msg;

    FILE_ERROR(String msg) {
        this.msg = msg;
    }

    public String getMsg() {

        return msg;
    }
}
