import java.io.File;
import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * @author : saif
 * @date : 23/03/15
 */
public class Test {

    public static void main(String[] args) {

        File dictFile = new File("/usr/share/dict/words");
        Dictionary dict = new Dictionary(dictFile);
        File fileToCheck = new File("/home/saif/Desktop/TestDoc");

        Document doc = new Document(fileToCheck,dict);


        //dict.printDict();

        //String word1 = "you’re";
        //String word2 = "you're";
        //System.out.println("\nContains [" + word1 + "] = " + dict.Check(word1));
        //System.out.println("\nContains [" + word2 + "] = " + dict.Check(word2));
        /*
        String test = "Hi! I'm a \"banana\".";
        System.out.println("\nTest String: " + test);

        ArrayList<String> list = new ArrayList<String>();
        Pattern punctuations = Pattern.compile("(?!\')\\p{Punct}", Pattern.UNICODE_CHARACTER_CLASS);
        String strippedTest = test.replaceAll(punctuations.toString(), "");
        for (int i = 0; i < strippedTest.split("\\s").length; i++) {
            list.add(strippedTest.split("\\s")[i]);
        }

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        */
    }
}
