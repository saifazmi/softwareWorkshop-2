/**
 * The Exit Codes for Spell Check
 * @author : saif
 * @date : 23/03/15
 */
public enum EXIT_CODE {

    NO_Error(0),
    SPELL_Error(1),
    IO_Error(2),
    FILE_Error(2);

    private int numVal;

    EXIT_CODE(int numVal) {
        this.numVal = numVal;
    }

    public int getNumVal() {
        return numVal;
    }
}
