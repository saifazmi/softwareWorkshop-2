import java.io.File;

/**
 * Performs Spelling Check on a given file
 * @author : saif
 * @date : 21/03/15
 */
public class SpellCheck {

    public static void main(String[] args) {

        if (args.length < 1) {
            System.err.println("USAGE: java SpellCheck <file to be checked>");
        }

        File dictFile;

        if (args.length == 1) {
            dictFile = new File("/usr/share/dict/words");
        } else {
            dictFile = new File(args[1]);
        }

        Dictionary dict = new Dictionary(dictFile);
        Document doc = new Document(new File(args[0]), dict);
    }
}
