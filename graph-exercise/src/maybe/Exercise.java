// DO NOT MODIFY this file.
// Any modification will incurr into the mark zero for the whole exercise.

package maybe;

class Exercise extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    Exercise() {
        super();
    }

    Exercise(String message) {
        super("Not implemented: " + message);
    }

}
