// WHAT (NOT) TO DO:
// -------------------
// REPLACE the "Exercise" lines with code (possibly empty code).
// LEAVE ALONE the "Exercise" lines for parts you are not solving.
//
// To get any marks for the whole exercise, your submission should
// compile using the "compile" script. Submissions that don't compile
// won't get any marks.

package maybe;

/**
 * Implementation of Nothing
 * (using the "composite pattern").
 */

public class Nothing<A> implements Maybe<A> {

    public Nothing() {
    }

    public boolean isNothing() {
        return true;
    }

    public int size() {
        return 0;
    }

    public String toString() {
        return "Nothing";
    }

    public boolean has(A a) {
        return false;
    }

    // Higher-order functions:

    public Maybe<A> filter(Predicate<A> p) {
        throw new Exercise("Nothing:filter");
    }

    public <B> Maybe<B> map(Function<A, B> f) {
        throw new Exercise("Nothing:map");
    }

    public <B> B fold(Function<A, B> f, B b) {
        throw new Exercise("Nothing:fold");
    }

    public boolean all(Predicate<A> p) {
        throw new Exercise("Nothing:all");
    }

    public boolean some(Predicate<A> p) {
        throw new Exercise("Nothing:some");
    }

    public void forEach(Action<A> f) {
        throw new Exercise("Nothing:forEach");
    }

    // Unsafe operations:
    public A fromMaybe() {
        throw new Exercise("Nothing:fromMaybe");
    }
}
