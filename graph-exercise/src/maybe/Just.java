// WHAT (NOT) TO DO:
// -------------------
// REPLACE the "Exercise" lines with code (possibly empty code).
// LEAVE ALONE the "Exercise" lines for parts you are not solving.
//
// To get any marks for the whole exercise, your submission should
// compile using the "compile" script. Submissions that don't compile
// won't get any marks.

package maybe;

/**
 * Implementation of Just
 * (using the "composite pattern").
 */

public class Just<A> implements Maybe<A> {

    private final A something;

    public Just(A something) {
        assert (something != null);
        this.something = something;
    }

    public boolean isNothing() {
        return false;
    }

    public int size() {
        return 1;
    }

    public String toString() {
        return "Just(" + something + ")";
    }

    public boolean has(A a) {
        return (something.equals(a));
    }

    // Higher-order functions:

    public Maybe<A> filter(Predicate<A> p) {
        throw new Exercise("Just:filter");
    }

    public <B> Maybe<B> map(Function<A, B> f) {
        throw new Exercise("Just:map");
    }

    public <B> B fold(Function<A, B> f, B b) {
        throw new Exercise("Just:fold");
    }

    public boolean all(Predicate<A> p) {
        throw new Exercise("Just:all");
    }

    public boolean some(Predicate<A> p) {
        throw new Exercise("Just:some");
    }

    public void forEach(Action<A> f) {
        throw new Exercise("Just:forEach");
    }

    // Unsafe operation:
    public A fromMaybe() {
        throw new Exercise("Just:fromMaybe");
    }
}
