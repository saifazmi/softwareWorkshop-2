package part3;

public interface IContainer<A> {
    public boolean isEmpty();

    public void addTo(A a);

    public A getFrom();
}
