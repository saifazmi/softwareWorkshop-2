package part3;

import java.util.LinkedList;

/**
 * An extension of Queue for generalisation.
 *
 * @param <A> the generic type of the collection
 */
public class EQueue<A> extends LinkedList<A> implements IContainer<A> {
    private static final long serialVersionUID = 1L;
    private LinkedList<A> queue;

    public EQueue() {
        super();
        this.queue = new LinkedList<A>();
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }

    public void addTo(A a) {
        queue.add(a);
    }

    public A getFrom() {
        return queue.remove();
    }
}
