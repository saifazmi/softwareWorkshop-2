package part3;

import ilist.Cons;
import ilist.IList;
import ilist.Nil;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import maybe.Just;
import maybe.Maybe;
import maybe.Nothing;
import maybe.Predicate;
import part2.Node;


public class Generalised<A> {

    private IContainer<Node<A>> container;
    private Set<Node<A>> visited;
    private Map<Node<A>, Node<A>> pathmap;

    /**
     * Constructing the data structure to perform the Generalised Search.
     *
     * @param container the data structure which determines the search; if Stack then DFS else if queue then BFS.
     */
    public Generalised(IContainer<Node<A>> container) {
        this.visited = new LinkedHashSet<Node<A>>();
        this.container = container;
        this.pathmap = new LinkedHashMap<Node<A>, Node<A>>();
    }


    /**
     * Returns the path produced by the Generalised Search
     *
     * @param start the starting node of the path.
     * @param end   the end node of the path.
     * @return IList of nodes in the order in which they are visited or Nothing if there is no path.
     */
    private IList<Node<A>> getPath(Node<A> start, Node<A> end) {

        IList<Node<A>> list = new Cons<Node<A>>(end, new Nil<Node<A>>());
        Node<A> z = end;

        while (!(z.equals(start))) {
            z = pathmap.get(z);
            list = new Cons<Node<A>>(z, list);
        }

        return list;

    }

    /**
     * Performs the Generalised Search.
     *
     * @param x starting point to perform the search.
     * @param p predicate for the search.
     * @return a Just IList of node(s) that form the path from x to Node which satisfies Predicate p or Nothing if there is no path.
     */
    public Maybe<IList<Node<A>>> findPathFrom(Node<A> x, Predicate<A> p) {

        visited.add(x);
        container.addTo(x);
        pathmap.put(x, x);

        while (!container.isEmpty()) {
            Node<A> y;
            Set<Node<A>> successors;
            y = container.getFrom();

            if (p.holds(y.contents())) {
                return new Just<IList<Node<A>>>(getPath(x, y));
            } else {
                successors = y.successors();
                Iterator<Node<A>> it = successors.iterator();
                while (it.hasNext()) {
                    Node<A> succ = it.next();
                    if (!visited.contains(succ)) {
                        visited.add(succ);
                        container.addTo(succ);
                        pathmap.put(succ, y);
                    }

                }

            }
        }
        return new Nothing<IList<Node<A>>>();
    }

}

