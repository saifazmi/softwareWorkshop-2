package part3;

import java.util.Stack;

/**
 * An extension of Stack for generalisation.
 *
 * @param <A> the generic type of the collection.
 */
public class EStack<A> extends Stack<A> implements IContainer<A> {
    private static final long serialVersionUID = 1L;
    private Stack<A> stack;

    public EStack() {
        super();
        this.stack = new Stack<A>();
    }

    public boolean isEmpty() {
        return stack.isEmpty();
    }

    public void addTo(A a) {
        stack.push(a);
    }

    public A getFrom() {
        return stack.pop();
    }

}
