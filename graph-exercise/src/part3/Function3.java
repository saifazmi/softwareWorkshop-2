package part3;

public interface Function3<A, C> {
    C apply(A a1, A a2);

}
