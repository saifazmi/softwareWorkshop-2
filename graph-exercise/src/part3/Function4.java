package part3;

public interface Function4<A, B> {
    B apply(A a, B b, A c);
}
