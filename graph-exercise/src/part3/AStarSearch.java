package part3;

import ilist.Cons;
import ilist.IList;
import ilist.Nil;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

import part2.Node;
import maybe.Just;
import maybe.Maybe;
import maybe.Nothing;

/**
 * Algorithm to perform a A-star Search
 *
 * @param <A> generic type of the collection
 */
public class AStarSearch<A> {
    private PriorityQueue<Node<A>> pending;
    private Set<Node<A>> visited;
    private Map<Node<A>, Double> D;
    private Map<Node<A>, Node<A>> pathmap;


    /**
     * Constructing the data structure required to perform A-star search.
     */
    public AStarSearch() {
        this.visited = new LinkedHashSet<Node<A>>();
        this.pathmap = new LinkedHashMap<Node<A>, Node<A>>();
        this.D = new LinkedHashMap<Node<A>, Double>();

    }

    /**
     * Returns the path produced by A-star search
     *
     * @param start The starting node of the path.
     * @param end   The end node of the path.
     * @return IList of nodes in the order in which they are visited or Nothing if there is no path.
     */
    private IList<Node<A>> getPath(Node<A> start, Node<A> end) {

        IList<Node<A>> list = new Cons<Node<A>>(end, new Nil<Node<A>>());
        Node<A> z = end;

        while (!(z.equals(start))) {
            z = pathmap.get(z);
            list = new Cons<Node<A>>(z, list);
        }

        return list;

    }

    /**
     * Performs A-star search.
     *
     * @param origin starting point of the search
     * @param goal   destination of the search
     * @param h      is the heuristic function, which is supposed to satisfy [h(x) <= d(x,y) + h(y)]
     * @param d      the distance function, Given two nodes, it computes the distance between the nodes' contents.
     * @return a Just IList of node(s) that forms the path from origin to goal in order or Nothing if there is no path.
     */
    public Maybe<IList<Node<A>>> findPathFrom(Node<A> origin, Node<A> goal, Function4<Node<A>, Double> h, Function3<Node<A>, Double> d) {

        pending = new PriorityQueue<Node<A>>();
        origin.setPriority(d.apply(origin, goal));
        pending.add(origin);

        pathmap.put(origin, origin);
        D.put(origin, 0.0);

        Node<A> n;
        Set<Node<A>> successors;
        while (!pending.isEmpty()) {
            n = pending.poll();

            if (n.contents().equals(goal.contents())) {
                return new Just<IList<Node<A>>>(getPath(origin, n));
            } else {
                visited.add(n);
                successors = n.successors();
                Iterator<Node<A>> it = successors.iterator();

                while (it.hasNext()) {
                    Node<A> succ = it.next();
                    if (!visited.contains(succ)) {
                        double cost = D.get(n) + d.apply(n, succ);
                        pathmap.put(succ, n);
                        if (!pending.contains(succ) || cost < D.get(succ)) {
                            D.put(succ, cost);
                            if (cost < D.get(succ)) {
                                succ.setPriority(h.apply(succ, D.get(succ), goal));
                            }
                            if (!pending.contains(succ)) {
                                succ.setPriority(h.apply(succ, D.get(succ), goal));
                                pending.add(succ);
                            }
                        }
                    }
                }
            }
        }
        return new Nothing<IList<Node<A>>>();
    }
}

	    




	

