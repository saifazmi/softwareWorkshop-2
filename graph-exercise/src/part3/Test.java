package part3;

import ilist.IList;

import part2.*;
import maybe.Maybe;
import maybe.Predicate;

/**
 * Test class for BFS and DFS
 */
public class Test {


    public static void main(String args[]) {

        int[][] nick = {
                {0, 0, 1, 0, 0, 1},
                {0, 1, 0, 0, 1, 1, 0, 2},
                {0, 2, 0, 3, 0, 1},
                {0, 3, 0, 2, 0, 4},
                {0, 4, 0, 3, 0, 5},
                {0, 5, 0, 6, 1, 5, 0, 4},
                {0, 6, 1, 6, 0, 5},
                {1, 0, 0, 0, 1, 1, 2, 0},
                {1, 1, 1, 2, 2, 1, 1, 0, 0, 1},
                {1, 2, 2, 2, 1, 1, 1, 3},
                {1, 3, 1, 2, 1, 4, 2, 3},
                {1, 4, 2, 4, 1, 5, 1, 3},
                {1, 5, 1, 4, 2, 5, 1, 6, 0, 5},
                {1, 6, 0, 6, 1, 5, 2, 6},
                {2, 0, 3, 0, 2, 1, 1, 0},
                {2, 1, 2, 2, 1, 1, 2, 0, 3, 1},
                {2, 2, 1, 2, 2, 1, 2, 3, 3, 2},
                {2, 3, 2, 2, 2, 4, 3, 3, 1, 3},
                {2, 4, 1, 4, 2, 5, 2, 3, 3, 4},
                {2, 5, 2, 4, 1, 5, 2, 6, 3, 5},
                {2, 6, 3, 6, 2, 5, 1, 6},
                {3, 0, 2, 0, 3, 1},
                {3, 1, 3, 0, 4, 1, 2, 1, 3, 2},
                {3, 2, 2, 2, 4, 2, 3, 1},
                {3, 3, 2, 3, 3, 4},
                {3, 4, 2, 4, 3, 3},
                {3, 5, 3, 6, 2, 5, 4, 5},
                {3, 6, 2, 6, 3, 5},
                {4, 0},
                {4, 1, 4, 2, 5, 1, 3, 1},
                {4, 2, 4, 1, 5, 2, 3, 2},
                {4, 3},
                {4, 4},
                {4, 5, 5, 5, 3, 5},
                {4, 6},
                {5, 0},
                {5, 1, 4, 1, 5, 2, 6, 1},
                {5, 2, 4, 2, 5, 1, 6, 2},
                {5, 3},
                {5, 4},
                {5, 5, 4, 5, 6, 5},
                {5, 6},
                {6, 0, 7, 0, 6, 1},
                {6, 1, 6, 0, 5, 1, 6, 2, 7, 1},
                {6, 2, 5, 2, 6, 1, 7, 2},
                {6, 3, 7, 3, 6, 4},
                {6, 4, 6, 3, 7, 4},
                {6, 5, 5, 5, 6, 6, 7, 5},
                {6, 6, 7, 6, 6, 5},
                {7, 0, 6, 0, 7, 1, 8, 0},
                {7, 1, 8, 1, 7, 0, 6, 1, 7, 2},
                {7, 2, 7, 3, 8, 2, 6, 2, 7, 1},
                {7, 3, 6, 3, 7, 2, 7, 4, 8, 3},
                {7, 4, 7, 3, 8, 4, 6, 4, 7, 5},
                {7, 5, 8, 5, 7, 6, 7, 4, 6, 5},
                {7, 6, 6, 6, 7, 5, 8, 6},
                {8, 0, 8, 1, 7, 0, 9, 0},
                {8, 1, 8, 2, 9, 1, 7, 1, 8, 0},
                {8, 2, 8, 1, 7, 2, 8, 3},
                {8, 3, 8, 2, 7, 3, 8, 4},
                {8, 4, 8, 5, 8, 3, 7, 4},
                {8, 5, 9, 5, 8, 4, 7, 5, 8, 6},
                {8, 6, 8, 5, 7, 6, 9, 6},
                {9, 0, 9, 1, 8, 0},
                {9, 1, 8, 1, 9, 2, 9, 0},
                {9, 2, 9, 1, 9, 3},
                {9, 3, 9, 2, 9, 4},
                {9, 4, 9, 5, 9, 3},
                {9, 5, 8, 5, 9, 4, 9, 6},
                {9, 6, 9, 5, 8, 6}
        };

        Graph<Coordinate> nicksGraph = new Graph<Coordinate>();

        for (int i = 0; i < nick.length; i++) {
            // What we are going to do relies on the two following facts
            // about nick:
            assert (nick[i].length >= 2);       // (1)
            assert (nick[i].length % 2 == 0);   // (2)

            int x = nick[i][0]; // Can't get array out of bounds
            int y = nick[i][1]; // because of assertion (1).
            Coordinate c = new Coordinate(x, y);
            Node<Coordinate> node = nicksGraph.nodeWith(c);

            // And next we add its successors. We rely on assertion (2)
            // again to avoid array out of bounds. Now we start from
            // position 2, as positions 0 and 1 have already been looked at
            // (they are x and y). Notice that we need to increment by 2.

            for (int j = 2; j < nick[i].length; j = j + 2) {
                int sx = nick[i][j];
                int sy = nick[i][j + 1];
                Coordinate sc = new Coordinate(sx, sy);
                Node<Coordinate> s = nicksGraph.nodeWith(sc);
                node.addSuccessor(s);
            }
        }

        DepthSearch<Coordinate> dfs = new DepthSearch<Coordinate>();
        BreadthFirstSearch<Coordinate> bfs = new BreadthFirstSearch<Coordinate>();

        //Coordinate start = new Coordinate(0, 0);
        //  Node<Coordinate> startingnode = nicksGraph.nodeWith(start);

        /**
         * Predicates
         * Can specify exact Coordinate
         * Predicate can be just a single x/y value  and hence it will
         be the first occurrence of this value found in a node
         * Can consist of 'Boolean operators' meaning one predicate could be satisfied
         by more than one node
         */
        //create predicate for bfs/dfs and generalised
        Predicate<Coordinate> predicate1 = new Predicate<Coordinate>() {
            @Override
            public boolean holds(Coordinate x) {
                Coordinate end = new Coordinate(3, 4);
                return (x.equals(end));
            }
        };

        /*
        Predicate<Coordinate> predicate2 = new Predicate<Coordinate>() {
            @Override
            public boolean holds(Coordinate x) {
                return (x.x == 1);
            }
        };
		*/

        //create heuristic and distance functions (anonymous class since we can't use lambda expressions)
        Function3<Node<Coordinate>, Double> d = new Function3<Node<Coordinate>, Double>() {
            @Override
            public Double apply(Node<Coordinate> n1, Node<Coordinate> n2) {
                return Math.sqrt(Math.pow(n1.contents().x - n2.contents().x, 2) + Math.pow(n1.contents().y - n2.contents().y, 2));
            }
        };


        Function4<Node<Coordinate>, Double> h = new Function4<Node<Coordinate>, Double>() {
            @Override
            public Double apply(Node<Coordinate> n1, Double distance, Node<Coordinate> goal) {
                return Math.sqrt(Math.pow(n1.contents().x - goal.contents().x, 2) + Math.pow(n1.contents().y - goal.contents().y, 2)) + distance;
            }
        };

        //create start node for path finding
        Coordinate start = new Coordinate(0, 0);
        Node<Coordinate> origin = nicksGraph.nodeWith(start);

//*** BFS and DFS  
        //create dfs and bfs objects so we can compare the generalised result
        Maybe<IList<Node<Coordinate>>> depthResult = dfs.findPathFrom(origin, predicate1);
        Maybe<IList<Node<Coordinate>>> breadthResult = bfs.findPathFrom(origin, predicate1);
        //print results from dfs and bfs
        System.out.println("The result from depth first search is: ");
        System.out.println(depthResult);
        System.out.println("The result from breadth first search is: ");
        System.out.println(breadthResult);


//*** A Star Search
        //create goal node for A* search
        Coordinate end = new Coordinate(9, 0);
        Node<Coordinate> goal = nicksGraph.nodeWith(end);
        //create a* search and print result
        AStarSearch<Coordinate> astar = new AStarSearch<Coordinate>();
        Maybe<IList<Node<Coordinate>>> astarresult = astar.findPathFrom(origin, goal, h, d);
        System.out.println("The result from A Star search is: ");
        System.out.println(astarresult);

//*** Generalisation
        //create objects for generalisation of bfs and dfs, then print results
        Generalised<Coordinate> genstack = new Generalised<Coordinate>(new EStack<Node<Coordinate>>());
        Generalised<Coordinate> genqueue = new Generalised<Coordinate>(new EQueue<Node<Coordinate>>());
        Maybe<IList<Node<Coordinate>>> genstackresult = genstack.findPathFrom(origin, predicate1);
        Maybe<IList<Node<Coordinate>>> genqueueresult = genqueue.findPathFrom(origin, predicate1);
        System.out.println("The result from generalised search using a stack:");
        System.out.println(genstackresult);
        System.out.println("the result from generalised search using a queue:");
        System.out.println(genqueueresult);


    }
}
