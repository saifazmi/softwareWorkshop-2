package part2;

/**
 * Stores a pair of values representing a coordinate.
 */
public class Coordinate {
    public int x, y;

    /**
     * Constructing the coordinate point
     *
     * @param x x-coord of the point
     * @param y y-coord of the point
     */
    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public int hashCode() {
        return x + y;
    }

    @Override
    public boolean equals(Object o) {
        Coordinate c = (Coordinate) o;
        return x == c.x && y == c.y;
    }

    public String toString() {
        return "(" + this.x + "," + this.y + ")";
    }
}
