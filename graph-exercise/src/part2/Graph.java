package part2;

import java.util.*;

// We represent a graph as a set of nodes. 
// This is a minimal class so that a graph can be created.

/**
 * A minimal representation of a graph
 *
 * @param <A> generic type of the graph
 */
public class Graph<A> {

    // Keep the implementation of sets open, by using the Set interface:
    private Set<Node<A>> nodes;

    /**
     * Constructs an empty graph
     */
    public Graph() {
        // Choose any implementation of sets you please, but you need to
        // choose one.
        nodes = new LinkedHashSet<Node<A>>();
    }

    /**
     * Gets the list of nodes.
     *
     * @return a list of nodes
     */
    public Set<Node<A>> nodes() {
        return nodes;
    }

    /**
     * Finds a node if it exists else creates it.
     *
     * @param c content of the node
     * @return a node with contents in question
     */
    public Node<A> nodeWith(A c) {
        for (Node<A> node : nodes) {  // Inefficient for large graph.
            if (node.contentsEquals(c))
                return node; // Found.
        }
        // Not found, hence create it:
        Node<A> node = new Node<A>(c);
        nodes.add(node);
        return node;
    }
}
