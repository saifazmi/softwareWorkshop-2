package part2;

import java.util.*;

// Minimal class for a particular implementation of directed graphs.
// All we include is what is necessary to build a graph, in the class
// graph.

/**
 * A minimal representation of a node
 *
 * @param <A> generic type of the node
 */
public class Node<A> implements Comparable<Node<A>> {

    private double priority;
    private A contents;
    // Keep the implementation of sets open, by using the Set interface:
    private Set<Node<A>> successors;

    /**
     * Constructs a node with empty successors.
     *
     * @param contents the node contents
     */
    public Node(A contents) {
        this.contents = contents;
        // Choose any implementation of sets you please, but you need to
        // choose one.
        this.successors = new LinkedHashSet<Node<A>>();
    }

    /**
     * Adds a successor to the node.
     *
     * @param s successive node
     */
    public void addSuccessor(Node<A> s) {
        successors.add(s);
    }

    /**
     * Compares the contents of the node.
     *
     * @param c contents to be compared
     * @return true if the contents are found else false
     */
    public boolean contentsEquals(A c) {
        return contents.equals(c);
    }

    /**
     * Gets the contents of node
     *
     * @return contents of node
     */
    public A contents() {
        return contents;
    }

    /**
     * Gets the successors of the node
     *
     * @return a set of successors
     */
    public Set<Node<A>> successors() {
        return successors;
    }

    /**
     * Prints the contents of the node
     *
     * @return contents of node in a string
     */
    public String toString() {
        return this.contents().toString();
    }

    public void setPriority(Double p) {
        this.priority = p;
    }

    public double getPriority() {
        return this.priority;
    }

    @Override
    public int compareTo(Node<A> n) {
        if (this.priority < n.getPriority()) {
            return -1;
        }
        if (this.priority > n.getPriority()) {
            return 1;
        } else return 0;
    }

}
