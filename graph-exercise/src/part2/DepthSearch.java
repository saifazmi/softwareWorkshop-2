package part2;

import ilist.Cons;
import ilist.IList;
import ilist.Nil;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import maybe.Just;
import maybe.Maybe;
import maybe.Nothing;
import maybe.Predicate;

/**
 * Algorithm to perform a Depth First Search
 *
 * @param <A> generic type of the collection
 */
public class DepthSearch<A> {

    private Stack<Node<A>> stack;
    private Set<Node<A>> visited;
    private Map<Node<A>, Node<A>> pathmap;

    /**
     * Constructing the data structure required to perform DFS.
     */
    public DepthSearch() {
        this.stack = new Stack<Node<A>>();
        this.visited = new LinkedHashSet<Node<A>>();
        this.pathmap = new LinkedHashMap<Node<A>, Node<A>>();
    }

    /**
     * Returns the path produced by DFS
     *
     * @param start The starting node of the path.
     * @param end   The end node of the path.
     * @return IList of nodes in the order in which they are visited or Nothing if there is no path.
     */
    private IList<Node<A>> getPath(Node<A> start, Node<A> end) {

        IList<Node<A>> list = new Cons<Node<A>>(end, new Nil<Node<A>>());
        Node<A> z = end;

        while (!(z.equals(start))) {
            z = pathmap.get(z);
            list = new Cons<Node<A>>(z, list);
            //list.append(z);
        }

        return list;

    }

    /**
     * Performs the DFS
     *
     * @param x starting point to perform the search.
     * @param p predicate for the search.
     * @return a Just IList of node(s) that form the path from x to Node which satisfies Predicate p or Nothing if there is no path.
     */
    public Maybe<IList<Node<A>>> findPathFrom(Node<A> x, Predicate<A> p) {

        visited.add(x);
        stack.push(x);
        pathmap.put(x, x);

        Node<A> y;
        Set<Node<A>> succ;
        Node<A> next;

        while (!stack.empty()) {
            y = stack.pop();
            if (p.holds(y.contents())) {
                return new Just<IList<Node<A>>>(getPath(x, y));
            } else {
                succ = y.successors();
                Iterator<Node<A>> it = succ.iterator();
                while (it.hasNext()) {
                    next = it.next();
                    if (!visited.contains(next)) {
                        visited.add(next);
                        stack.push(next);
                        pathmap.put(next, y);
                    }
                }
            }

        }
        return new Nothing<IList<Node<A>>>();
    }

}
